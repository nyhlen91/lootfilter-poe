# README #
Lootfilter for Path of exile

##TODO

1. Divination cards, highlighta vissa nya (nurse)

### Changelog ###

v.2.4 (Wisdom scrolls hidden)

1. Fixed essences

2. Added some div cards

3. Colorcoded borders for maps

v.2.3 (Wisdom scrolls hidden)

1. Added Fossils

2. Orb of annulment moved to higher valued currency,

3. Chayula splinters got minimap icon

v.2.2

1.  Added Legion currency

v.2.1

1. Added abyss jewels

2. Added Shaper/elder items


v.2.0

1. Removed lowtier esbases

2. Removed sound from high tier esbases

3. Added new currency

4. Updated divination cards

v.1.5

1. Edited for mid league

2. Added b version with different chance base

V.1.4

1. Edited for early league

2. Changed highlighting of new bases

3. Changed priority of new bases

4. Added leaguestones

v.1.3

1. Added support identified blue items(warband items)

2. Changed sword priority in high level

v.1.2

1. hidden wisdom scrolls

2. removed sound from many white items

3. changed color for a couple of uninteresting rares